using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILobbyPlayer : MonoBehaviour {

    [SerializeField] Image readyButtonImage;

    bool ready = false;

    public void ReadyPlayer () {
        ready = !ready;
        Player.local.ReadyPlayer (ready);

        readyButtonImage.color = ready ? Color.green : Color.grey;
    }

}