using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIGameState : MonoBehaviour {

    [SerializeField] TMP_Text gameStateText;
    [SerializeField] TMP_Text countdownText;

    void Awake () {
        Player.OnGameStateUpdated.AddListener (GameStateUpdated);
        Player.OnCountdownUpdated.AddListener (CountdownUpdated);
    }

    void OnDestroy () {
        Player.OnGameStateUpdated.RemoveListener (GameStateUpdated);
        Player.OnCountdownUpdated.RemoveListener (CountdownUpdated);
    }

    void GameStateUpdated (GameState gameState) {
        gameStateText.text = gameState.ToString ();
    }

    void CountdownUpdated (int time) {
        countdownText.text = time.ToString ("00");

        if (time == 0) StartCoroutine (HideCountDownText ());
    }

    IEnumerator HideCountDownText () {
        yield return new WaitForSeconds (1);
        countdownText.text = string.Empty;
    }

}