using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputController {

    public static UnityEvent<Vector2> OnMovePerformed = new UnityEvent<Vector2> ();
    public static UnityEvent OnMoveCanceled = new UnityEvent ();

    public static UnityEvent OnDroppingPerformed = new UnityEvent ();
    public static UnityEvent OnInteractPerformed = new UnityEvent ();

    public static Vector2 moveVector;

    GameControls gameControls;

    public InputController () {
        gameControls = new GameControls ();
        RegisterInputs ();
    }

    public void Enable () {
        gameControls.Enable ();
    }

    public void Disable () {
        gameControls.Disable ();
    }

    public void Dispose () {
        gameControls.Dispose ();
    }

    void RegisterInputs () {
        gameControls.Gameplay.Move.performed += x => {
            moveVector = x.ReadValue<Vector2> ();
            OnMovePerformed.Invoke (moveVector);
        };

        gameControls.Gameplay.Move.canceled += x => {
            moveVector = Vector2.zero;
            OnMoveCanceled.Invoke ();
        };

        gameControls.Gameplay.Dropping.performed += x => {
            OnDroppingPerformed.Invoke ();
        };

        gameControls.Gameplay.Interact.performed += x => {
            OnInteractPerformed.Invoke ();
        };
    }

}