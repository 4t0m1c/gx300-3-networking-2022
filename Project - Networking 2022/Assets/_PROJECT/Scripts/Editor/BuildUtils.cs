using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public static class BuildUtils {

    public static bool shouldStripShaders = false;

    [MenuItem ("Build Tools/DEPLOY", false, 99)]
    static void BuildForDeployment () {
        BuildLinuxServer ();
        BuildWindowsClient ();
    }

    [MenuItem ("Build Tools/Server [LINUX]")]
    static void BuildLinuxServer () {
        EditorUserBuildSettings.SwitchActiveBuildTarget (BuildTargetGroup.Standalone, BuildTarget.StandaloneLinux64);

        string path = Path.GetFullPath ($"{Application.dataPath}/../Build/{Application.version}/Server [LINUX]/");
        string filename = $"{Application.productName}[SERVER].x86_64";

        List<string> scenesToBuild = new List<string> ();
        foreach (var scene in EditorBuildSettings.scenes) {
            if (scene.enabled) {
                scenesToBuild.Add (scene.path);
            }
        }
        string[] levels = scenesToBuild.ToArray ();

        shouldStripShaders = true;

        BuildPipeline.BuildPlayer (new BuildPlayerOptions () {
            locationPathName = path + filename,
                scenes = levels,
                target = BuildTarget.StandaloneLinux64,
                subtarget = (int) StandaloneBuildSubtarget.Server,
                extraScriptingDefines = new string[] { "UNITY_SERVER" }
        });

        shouldStripShaders = false;

        System.Diagnostics.Process.Start (path);
        EditorUserBuildSettings.SwitchActiveBuildTarget (BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
    }

    [MenuItem ("Build Tools/Server [WIN]")]
    static void BuildWindowsServer () {
        EditorUserBuildSettings.SwitchActiveBuildTarget (BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);

        string path = Path.GetFullPath ($"{Application.dataPath}/../Build/{Application.version}/Server [WIN]/");
        string filename = $"{Application.productName}[SERVER].exe";

        List<string> scenesToBuild = new List<string> ();
        foreach (var scene in EditorBuildSettings.scenes) {
            if (scene.enabled) {
                scenesToBuild.Add (scene.path);
            }
        }
        string[] levels = scenesToBuild.ToArray ();

        shouldStripShaders = true;

        BuildPipeline.BuildPlayer (new BuildPlayerOptions () {
            locationPathName = path + filename,
                scenes = levels,
                target = BuildTarget.StandaloneWindows64,
                subtarget = (int) StandaloneBuildSubtarget.Server,
                extraScriptingDefines = new string[] { "UNITY_SERVER" }
        });

        shouldStripShaders = false;

        System.Diagnostics.Process.Start (path);
    }

    [MenuItem ("Build Tools/Client [WIN]")]
    static void BuildWindowsClient () {
        EditorUserBuildSettings.SwitchActiveBuildTarget (BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);

        string path = Path.GetFullPath ($"{Application.dataPath}/../Build/{Application.version}/Client [WIN]/");
        string filename = $"{Application.productName}[CLIENT].exe";

        List<string> scenesToBuild = new List<string> ();
        foreach (var scene in EditorBuildSettings.scenes) {
            if (scene.enabled) {
                scenesToBuild.Add (scene.path);
            }
        }
        string[] levels = scenesToBuild.ToArray ();

        BuildPipeline.BuildPlayer (new BuildPlayerOptions () {
            locationPathName = path + filename,
                scenes = levels,
                target = BuildTarget.StandaloneWindows64
        });

        System.Diagnostics.Process.Start (path);
    }

}