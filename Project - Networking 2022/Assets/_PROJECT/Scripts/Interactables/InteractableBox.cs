using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class InteractableBox : NetworkBehaviour {

    bool canInteract = true;
    [SerializeField] Rigidbody rigidbody;

    public void Interact (PlayerCharacter playerCharacter) {
        if (canInteract) {
            Debug.Log ($"[{gameObject.name}] Interacted by [{playerCharacter.netId}]");
            playerCharacter.Pickup (this);
        }
    }

    [Server]
    public void SetCanInteract (bool _canInteract) {
        canInteract = _canInteract;
        RpcSetCanInteract (_canInteract);
    }

    [ClientRpc]
    void RpcSetCanInteract (bool _canInteract) {
        canInteract = _canInteract;
    }

    [Server]
    public void YeetTheBox (Vector3 direction, float force) {
        rigidbody.AddForce (direction * force, ForceMode.Impulse);
    }

    [Server]
    public void PickedUp () {
        rigidbody.isKinematic = true;
        transform.localScale = Vector3.one / 2;
        RpcPickedUp ();
    }

    [ClientRpc]
    void RpcPickedUp () {
        rigidbody.isKinematic = true;
        transform.localScale = Vector3.one / 2;
    }

    [Server]
    public void Dropped () {
        rigidbody.isKinematic = false;
        transform.localScale = Vector3.one;
        RpcDropped ();
    }

    [ClientRpc]
    void RpcDropped () {
        rigidbody.isKinematic = false;
        transform.localScale = Vector3.one;
    }

}