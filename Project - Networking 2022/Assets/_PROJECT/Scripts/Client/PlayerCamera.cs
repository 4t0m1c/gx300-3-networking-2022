using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerCamera : MonoBehaviour {

    public CinemachineVirtualCamera virtualCamera;

    void Start() {
        virtualCamera.m_Follow = PlayerCharacter.local.transform; 
        virtualCamera.m_LookAt = PlayerCharacter.local.transform; 
    }

}