using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveZone : MonoBehaviour {

    [SerializeField] List<InteractableBox> validBoxes = new List<InteractableBox> ();

    void Start () {
        if (NetworkManager.Instance.mode != Mirror.NetworkManagerMode.ServerOnly) {
            enabled = false;
        }
    }

    //Server
    void OnCollisionEnter (Collision other) {
        if (other.collider.attachedRigidbody != null && other.collider.attachedRigidbody.TryGetComponent (out InteractableBox interactableBox)) {
            if (!validBoxes.Contains (interactableBox)) {
                Debug.Log ($"Box collided with zone");
                validBoxes.Add (interactableBox);
                interactableBox.SetCanInteract (false);
            }
        }
    }

}