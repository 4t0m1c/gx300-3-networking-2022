using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.Events;

public class NetworkManager : Mirror.NetworkManager {

    public static NetworkManager Instance;

    [SerializeField] GameObject lobbyManagerPrefab;

    UnityAction OnSceneLoaded;

    public override void Start () {
        base.Start ();
        Instance = this;

        if (Application.isBatchMode) {
            StartServer ();
        }

#if UNITY_EDITOR
        networkAddress = "localhost";
#endif
    }

    public override void OnStartServer () {
        GameObject lobbyManager = Instantiate (lobbyManagerPrefab);
        NetworkServer.Spawn (lobbyManager);
    }

    public void SwitchScene (string sceneName, UnityAction OnSceneLoaded) {
        this.OnSceneLoaded = OnSceneLoaded;
        ServerChangeScene (sceneName);
    }

    public override void OnServerSceneChanged (string sceneName) {
        OnSceneLoaded?.Invoke ();
        OnSceneLoaded = null;
    }

}